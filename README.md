# README #

JavaFX app to store your password in encrypted file. Encryption is all in Crypt.java if you want to take a look. passphrase => PKDF => AES (256 bit)

No external dependencies but you might need Java unlimited cryptographic extensions policy file (oracle links to this might change so search in your favorite search
engine for Java unlimited cryptography extensions, place them in $JAVE_HOME/jre/lib/security.

### What is this repository for? ###

* Use keeppass
* Use lastpass
* People who don't mind losing all their saved passwords when they forget their master password
* People who want to keep an ecrypted text file in their $CLOUD_STORAGE provider folder and want to decrypt their passwords in a GUI (also you can use openssl
  or have some way to do same in vim/emacs I'm sure)

### How do I get set up? ###

* clone repo
* cd $repo
* mvn pacakage
* java -jar target/pass-keep-1.0-SNAPSHOT.jar

### How it works ###
* Create a file somewhere on your computer
* Run passkeep - choose that file
* Enter a password that will be used to encrypt file
* Open file
* Will end up with dialog like below
* ![screenshot] (https://bytebucket.org/ashwinamit/passkeep/raw/1d1b6903a012a0dd9c28ee971bacf18af332e1e1/pass-keep.png?token=7985bd75cc7eb662a0156ae4d934756ca99adf79)

1. Extra special characters (beyond [A-Za-z0-9] to use in password generation
2. Text field where password will be generated
3. Respectively the site and k,v pair to store for that site (teamliquid would be site)
    1. Normally k,v would be username/pass but might also be secret key/secret answer
4. *Important:* you must save file - simply adding your passwods won't do anything till a save.
5. Clears fields in (3)
6. When you want to rotate your master password, you simply choose a new file to save passwords and save it with password in (7)

* *How to update a password?*: Just saw a new password - it's backed by a Map so it will simply overwrite old password
* *How do a view a stored password?:* Click on a stored row and it will populate the fields in (3)