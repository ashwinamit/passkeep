package com.example.passkeep;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

public class Crypt {

    private static final String DELIMITER = ";"; //not part of base64 alphabet so ok to delimit
    private static final int SALT_POS = 0;
    private static final int IV_POS = 1;
    private static final int CIPHERTEXT_POS = 2;
    private static SecureRandom sr;


    static {
        sr = new SecureRandom();
    }

    public static void main(String[] args) throws GeneralSecurityException, UnsupportedEncodingException {
        Crypt c = new Crypt();
        String pw = "password";
        String plain = "test";

        //Encrypt
        String encrypted = encrypt(plain, pw);
        System.out.println(encrypted);

        //Decrypt
        Crypt c2 = new Crypt();
        System.out.println(decrypt(encrypted, pw));
    }

    private static byte[] getSalt() {
        byte[] salt = new byte[8];
        sr.nextBytes(salt);
        return salt;
    }

    private static SecretKey getKey(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 256);
        SecretKey tmp = factory.generateSecret(spec);
        return new SecretKeySpec(tmp.getEncoded(), "AES");
    }

    public static String encrypt(String text, String password) throws GeneralSecurityException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] salt = getSalt();
        cipher.init(Cipher.ENCRYPT_MODE, getKey(password, salt));

        AlgorithmParameters params = cipher.getParameters();
        byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
        byte[] ciphertext = cipher.doFinal(text.getBytes("UTF-8"));

        Base64.Encoder encoder = Base64.getEncoder();
        String saltstr = encoder.encodeToString(salt);
        String ivstr = encoder.encodeToString(iv);
        String cipherstr = encoder.encodeToString(ciphertext);
        return saltstr + DELIMITER + ivstr + DELIMITER + cipherstr;
    }

    public static String decrypt(String cipherText, String password) throws GeneralSecurityException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        Base64.Decoder decoder = Base64.getDecoder();
        String[] parts = cipherText.split(DELIMITER);
        byte[] salt = decoder.decode(parts[SALT_POS]);
        byte[] iv = decoder.decode(parts[IV_POS]);
        cipher.init(Cipher.DECRYPT_MODE, getKey(password, salt), new IvParameterSpec(iv));
        byte[] cipherBytes = decoder.decode(parts[CIPHERTEXT_POS]);
        return new String(cipher.doFinal(cipherBytes), "UTF-8");
    }
}
