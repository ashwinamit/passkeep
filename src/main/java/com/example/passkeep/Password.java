package com.example.passkeep;

import java.security.SecureRandom;

/**
 * Created by ashwin on 2/28/2015.
 */
public class Password {
    private String site;
    private String username;
    private String password;

    public Password(String site, String username, String password) {
        this.site = site;
        this.username = username;
        this.password = password;
    }

    public static String generatePassword(int minLength, int maxLength, String specialAlphabet) {
        String basicAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"; //A-Za-z0-9
        String fullAlphabet = basicAlphabet + specialAlphabet;
        String output = ""; //passwords shouldn't be too long, forget StringBuffer
        SecureRandom sr = new SecureRandom();
        int pwLength = minLength + sr.nextInt(maxLength - minLength);
        for (int i = minLength; i < pwLength; i++) {
            output += fullAlphabet.charAt(sr.nextInt(fullAlphabet.length()));
        }
        return output;

    }

    public String getSite() {
        return site;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}