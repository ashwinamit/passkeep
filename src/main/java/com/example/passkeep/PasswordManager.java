package com.example.passkeep;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by ashwin on 2/28/2015.
 */
public class PasswordManager {

    private static final String FIELD_SEPARATOR = "\t";
    private static final String RECORD_SEPARATOR = "\n";
    private Map<String, Password> passwords;

    private String password;
    private File backingFile;

    public PasswordManager() {
        this.passwords = new HashMap<>();
    }


    public static PasswordManager loadFromEncryptedFile(String password, File file) throws IOException, GeneralSecurityException {
        PasswordManager pm;
        byte[] bytes = Files.readAllBytes(file.toPath());
        String encryptedContent = new String(bytes, StandardCharsets.UTF_8);
        if (file.exists() && !encryptedContent.isEmpty()) {


            String plainContent = Crypt.decrypt(encryptedContent, password);

            Scanner fileScanner = new Scanner(plainContent);
            pm = new PasswordManager();
            while (fileScanner.hasNextLine()) {
                String line = fileScanner.nextLine();
                String[] parts = line.split(FIELD_SEPARATOR);
                if (parts.length == 3) {
                    Password p = new Password(parts[0], parts[1], parts[2]);
                    pm.addPassword(p);
                }

            }
        } else {
            pm = new PasswordManager();
        }
        pm.password = password;
        pm.backingFile = file;
        return pm;
    }

    public static void main(String[] args) throws GeneralSecurityException, IOException {
        PasswordManager pm = new PasswordManager();
        pm.addPassword("Test", "U1", "Secret");
        pm.addPassword("Test2", "U2", "Secret2");
        File out = new File("tmp.txt");
        out.createNewFile();
        pm.writeToFileEncrypted("password", out);

        PasswordManager pm2 = PasswordManager.loadFromEncryptedFile("password", out);
        System.out.println(pm2);
        out.delete();
    }

    public void writeToFileEncrypted(String password, File file) throws GeneralSecurityException, IOException {
        try (FileWriter fw = new FileWriter(file)) {
            String plainText = toString();
            String encrypted = Crypt.encrypt(plainText, password);
            fw.write(encrypted);
            fw.flush();
            fw.close();
        }
    }

    public void writeToFileEncrypted() throws GeneralSecurityException, IOException {
        writeToFileEncrypted(this.password, this.backingFile);
    }

    public void addPassword(Password p) {
        this.passwords.put(p.getSite(), p);
    }

    public void addPassword(String site, String username, String password) {
        this.passwords.put(site, new Password(site, username, password));
    }

    public Password getPassword(String site) {
        return passwords.get(site);
    }

    public String toString() {
        StringBuilder output = new StringBuilder();
        for (Password p : passwords.values()) {
            output.append(String.format("%s%s%s%s%s%s", p.getSite(), FIELD_SEPARATOR, p.getUsername(), FIELD_SEPARATOR,
                    p.getPassword(), RECORD_SEPARATOR));
        }
        return output.toString();
    }

    public Collection<String> getSites() {
        return passwords.keySet();
    }
}
