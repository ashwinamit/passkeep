package com.example.passkeep;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Created by ashwin on 2/28/2015.
 */
public class FileChooserController {

    @FXML
    private Parent root;

    @FXML
    private Label choosenFileLabel, errorMessage;

    @FXML
    private TextField passwordText;

    private File choosenFile;

    public void showFileChooser(ActionEvent actionEvent) {
        final FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(root.getScene().getWindow());
        if (file != null) {
            this.choosenFile = file;
            this.choosenFileLabel.setText(file.getName());
        }
    }

    public void loadManager(ActionEvent actionEvent) throws IOException {
        loadManager();
    }

    public void loadManagerEnter(KeyEvent event) throws IOException {
        loadManager();
    }

    private void loadManager() throws IOException {
        if (choosenFile == null) {
            errorMessage.setText("Select a file");
            return;
        }
        FXMLLoader loader = new FXMLLoader(getClass().getResource("password_list.fxml"));
        Parent newRoot = loader.load(); //need to perform load else controller will be null
        PasswordListController controller = loader.getController();

        try {
            controller.setPasswordManager(choosenFile, passwordText.getText());
        } catch (IOException e) {
            errorMessage.setText("IO Error: " + e.getMessage());
            return;
        } catch (GeneralSecurityException e) {
            errorMessage.setText("Security Error: " + e.getMessage());
            return;

        }

        Scene newScene = new Scene(newRoot);
        Stage stage = (Stage) root.getScene().getWindow();
        stage.setTitle("PassKeep");
        stage.setScene(newScene);
        stage.show();
    }

}
