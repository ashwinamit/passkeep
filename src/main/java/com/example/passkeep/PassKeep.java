package com.example.passkeep;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by ashwin on 2/28/2015.
 */
public class PassKeep extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("choose_file.fxml"));
        Scene scene = new Scene(root);

        stage.setTitle("PassKeep");
        stage.setScene(scene);
        stage.show();
    }
}
