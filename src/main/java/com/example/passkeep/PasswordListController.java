package com.example.passkeep;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.Instant;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by ashwin on 2/28/2015.
 */
public class PasswordListController {

    @FXML
    private Parent root;

    @FXML
    private ListView<String> passwordList;

    @FXML
    private Label errorMessage, saveProgress, newFileLabel;

    @FXML
    private TextField specialAlphabet, minLength, maxLength, generatedPassword, newSite, newUser, newPassword, newFilePass;

    private PasswordManager passwordManager;
    private ObservableList<String> names;
    private File newFile;

    public void setPasswordManager(File file, String password) throws GeneralSecurityException, IOException {
        passwordManager = PasswordManager.loadFromEncryptedFile(password, file);
        names = FXCollections.observableArrayList();
        names.addAll(passwordManager.getSites());
        Collections.sort(names, Comparator.<String>naturalOrder());
        passwordList.setItems(names);
        passwordList.setOnMouseClicked(event -> {
            String selectedSite = passwordList.getSelectionModel().getSelectedItem();
            Password p = passwordManager.getPassword(selectedSite);
            newSite.setText(p.getSite());
            newUser.setText(p.getUsername());
            newPassword.setText(p.getPassword());
        });
    }

    public void generatePassword(ActionEvent actionEvent) {
        try {
            int min = Integer.parseInt(minLength.getText());
            int max = Integer.parseInt(maxLength.getText());
            generatedPassword.setText(Password.generatePassword(min, max, specialAlphabet.getText()));
        } catch (NumberFormatException e) {
            errorMessage.setText("Number parsing exception: " + e.getMessage());
        }
    }

    public void addPassword(ActionEvent actionEvent) {
        names.clear();
        passwordManager.addPassword(newSite.getText(), newUser.getText(), newPassword.getText());
        names.addAll(passwordManager.getSites());
        Collections.sort(names, Comparator.<String>naturalOrder());
        clear();
    }

    public void save(ActionEvent actionEvent) {
        save();
    }

    public void saveEnter(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            save();
        }
    }

    public void clear(ActionEvent event) {
        clear();
    }

    public void clearKey(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            clear();
        }

    }

    public void clear() {
        newSite.setText("");
        newUser.setText("");
        newPassword.setText("");
    }

    private void save() {
        try {
            passwordManager.writeToFileEncrypted();
            saveProgress.setText("Save complete! " + Instant.ofEpochMilli(System.currentTimeMillis()));
        } catch (IOException e) {
            errorMessage.setText("IO Error: " + e.getClass());
        } catch (GeneralSecurityException e) {
            errorMessage.setText("Security Exception: " + e.getMessage());
        }
    }

    public void newFileChooser(ActionEvent actionEvent) {
        final FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(root.getScene().getWindow());
        if (file != null) {
            this.newFile = file;
            this.newFileLabel.setText(file.getName());
        }
    }

    public void saveToNewFile(ActionEvent actionEvent) {
        if (!newFilePass.getText().isEmpty() && this.newFile != null) {
            try {
                passwordManager.writeToFileEncrypted(newFilePass.getText(), newFile);
                saveProgress.setText("Save complete: " + System.currentTimeMillis());
            } catch (IOException e) {
                errorMessage.setText("IOException: " + e.getMessage());
            } catch (GeneralSecurityException e) {
                errorMessage.setText("Security exception: " + e.getMessage());
            }
        } else {
            errorMessage.setText("Choose file and new password to save");
        }
    }
}
