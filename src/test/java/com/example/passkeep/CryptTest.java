package com.example.passkeep;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Unit test for simple App.
 */
public class CryptTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public CryptTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(CryptTest.class);
    }

    public void testCrypt() {
        String text = "Test";
        String password = "password";
        String decrypted = "";
        try {
            String encrypted = Crypt.encrypt(text, password);
            decrypted = Crypt.decrypt(encrypted, password);
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
        assertEquals(text, decrypted);
    }

    public void testPasswordManager() {
        PasswordManager pm = new PasswordManager();
        pm.addPassword("site1", "user1", "password1");
        pm.addPassword("site2", "user2", "password2");

        String password = "password";

        PasswordManager pm2 = null;

        try {
            Path file = Files.createTempFile(null, null);
            pm.writeToFileEncrypted(password, file.toFile());
            pm2 = PasswordManager.loadFromEncryptedFile(password, file.toFile());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
        assertEquals(pm.toString(), pm2.toString());
    }
}
